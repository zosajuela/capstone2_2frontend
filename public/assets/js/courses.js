// console.log("hello from JS")
let modalButton = document.querySelector('#adminButton')

let modalBut = document.querySelector('#buttonAdmin')
//CAPTURE the html body which will display the content coming from the db.
let container = document.querySelector('#coursesContainer')
let cardFooter; 
//we are going to take the value of the isAdmin property from the local storage.
let isAdmin = localStorage.getItem("isAdmin")


if(isAdmin == "false" || !isAdmin){
   //if a user is a regular user, do not show the addcourse button.
   modalButton.innerHTML = null; 
}else{
    modalButton.innerHTML = `
    <a href="https://evening-fjord-91994.herokuapp.com" target="_blank"><i class="fa fa-folder"></i><span>   S.M.E</span></a>
    `
}

if(isAdmin == "false" || !isAdmin){
   //if a user is a regular user, do not show the addcourse button.
   modalBut.innerHTML = null; 
}else{
   modalBut.innerHTML = `
    <a href="./addCourse.html"><i class="fa fa-graduation-cap"></i><span>   Add a Course</span></a>
    `
}




fetch('https://evening-fjord-91994.herokuapp.com/api/courses/').then(res => res.json()).then(data => {
	console.log(data); 
	//declare a variable that will display a result in the browser depending on the return
	let courseData; 
	//create a control structure that will determine the value that the variable will hold. 
	if(data.length < 1){
        courseData = "No Course Available"
	}else{
       //we will iterate the courses collection and display each course inside the browser
       courseData = data.map(course => {
       	//lets check the make up of each element inside the courses collection
       	console.log(course._id); 

        //if the user is a regular user, display the enroll button and display course button.
        if(isAdmin == "false" || !isAdmin)
        {
           cardFooter = ` <a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">View Course details</a>`
        }else{
           cardFooter = `
              <a id="deleted" href="./daddCourse.html?courseId=${course._id}" class="nav-link"> Edit </a>
              <a id="deleted" href="./deleteCourse.html?courseId=${course._id}" class="nav-link"> Delete </a>
              <a id="deleted" href="./course.html?courseId=${course._id}" class="nav-link"> View </a>
           `
        }
        
      //  
       	return (
       		`
       		<div class="card" id="delete">
                        <div class="box" id="adminButton">
                            <div class="content">
                                   <h4 class="card-title">${course.name}</h4>
                                   <h5 class="card-text">Per Semister Course Price: ₱</h5>
                                   
                                   <p>${course.price}</p>
                                   <br>
                                   <img class="logo-med" src="../assets/images/logo2.png" alt="">
                                   
                                     

                                     <p>${course.createdOn}</p>
                                       <p>Click View to see details of this Course</p>
                                     <p>${cardFooter}</p>
                                  </div>
                              </div>
                       </div>`
					   
       		)
       }).join("")//we used the join() to create a return of a new string
       //it "contatenated" all the objects inside the array and converted each to a string data type. 
	}
	container.innerHTML = courseData; 
})