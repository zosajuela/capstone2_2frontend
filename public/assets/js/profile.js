let token = localStorage.getItem("token")
console.log(token)

let cardFooter = `<button id="delete">Delete</button>`;

let profile = document.querySelector('#profileContainer') 

//lets create a control structure that will determin the display if the access token is null or empty.
if(!token || token === null){
   //lets redirect the user to the login page
   alert("You must Login First");
   window.location.href="./login.html"
}else{
   //console.log("yes may nakuhang token")//for educational purposes only
   fetch('http://localhost:4000/api/users/details', {
   	  method: 'GET',
   	  headers: {
   	  	'Content-Type': 'application/json',
   	  	'Authorization': `Bearer ${token}`//use template literals
   	  }
   }).then(res => res.json())
   .then(data => {
   	  console.log(data); //checking purposes

      let enrollmentData = data.enrollments.map(classData => {
         console.log(classData)
         return(
             `
               <tr>
                 <td>${classData.courseId}</td>
				 <td>${classData.enrolledOn}</td>
				 <td>${classData.name}</td>
				 <td>${classData.status}</td>
				 <td>${cardFooter}</td>
               </tr>
             `
          )
      }).join("") //remove the commas
   	  profile.innerHTML = `
        <div class="col-md-12">
        	<section class="jumbotron my-5">
        		<h3 class="text-center">First Name:  ${data.firstName}</h3>
        		<h3 class="text-center">Last Name:  ${data.lastName}</h3>
        		<h3 class="text-center">Email: ${data.email}  </h3>
        		<table class="table">
        			<thead>
        				<tr>
        					<th>Course ID:</th>
        					<th>Enrolled On:</th>
							<th>Name: </th>
        					<th>Status: </th>
        					<tbody> ${enrollmentData} </tbody>
        				</tr>
        			</thead>
        		</table>
        	</section>
        </div>

   	  `
			
	
		//  document.getElementById('#delete').addEventListener("click", (e) =>{
		// 	e.preventDefault();
		// 	const deleteMethod = {
		// 		method: 'DELETE', // Method itself
		// 		headers: {
		// 		 'Content-type': 'application/json; charset=UTF-8' // Indicates the content 
		// 		}
		// 		fetch(` http://localhost:4000/api/courses/${courseId}`)
		// 	}
		//  })
   })
}
