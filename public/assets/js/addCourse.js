let createCourse = document.querySelector("#createCourse");
        
// e=event
createCourse.addEventListener("submit", (e) => {
    e.preventDefault();

    let courseName = document.querySelector("#courseName").value;
    let coursePrice = document.querySelector("#coursePrice").value;
    let courseDescription = document.querySelector("#courseDescription").value;


    //fetch() has 2 params => route(destination)
    if(courseName !== "" && coursePrice !== "" && courseDescription !== ""){
        fetch('https://evening-fjord-91994.herokuapp.com/api/courses/name-exists',{
            method: 'POST',
            headers:{
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: courseName
            })
            }).then(res => res.json()).then(data => {
             if(data === false){
                  fetch("https://evening-fjord-91994.herokuapp.com/api/courses/addCourse",{
                     //built a structure of our request
                      method: 'POST', 
                      headers: {
                             'Content-Type': 'application/json'
                  },
                           body: JSON.stringify({
                            name: courseName,
                            description: courseDescription, 
                            price: coursePrice,
                  })
                 //create a promise to identify what will happen if res from back end is positive or nega
                    }).then(res => {
                           return res.json()
                        }).then(data => {
                              console.log(data)
                               if(data === true){
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Congratulations!',
                                    text: 'you have successfully created a profile'
                                });
                           } else{
                            Swal.fire({
                                icon: 'error',
                                title: 'Ooops!',
                                text: 'something went wrong'
                            });
                          }
                     })
            }else{
                Swal.fire({
					icon: 'warning',
					title: 'Course already exists',
					text: 'choose another Course Name'
				});
            }
          })
    }else{
        Swal.fire({
			icon: 'error',
			title: 'Ooops!',
			text: 'make sure everything is filled up correctly'
		});
    }
})