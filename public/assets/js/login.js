// console.log("hello fromn JS"); 

let loginForm = document.querySelector('#loginUser')


loginForm.addEventListener("submit", (e) => {
   e.preventDefault()

   let email = document.querySelector("#userEmail").value
   console.log(email)
   let password = document.querySelector("#password").value 
   console.log(password) 

   //how can we inform a user that a blank input field cannot be logged in?
   if(email == "" || password == ""){
      Swal.fire({
         icon: 'error',
         title: 'Ooops!',
         text: 'Please dont leave input fields blank'
      });
   }else{
      fetch('https://evening-fjord-91994.herokuapp.com/api/users/login', {
      	method: 'POST',
      	headers: {
      		'Content-Type': 'application/json'
      	},
      	body: JSON.stringify({
      		email: email,
      		password: password
      	})
      }).then(res => {
      	return res.json()
      }).then(data => {
      	console.log(data.access)
      	if(data.access){
           //lets save the access token inside our local storage.
           localStorage.setItem('token', data.access)
           //this local storage can be found inside the subfolder of the appData of the local file of the google chrome browser inside the data module of the user folder.
           Swal.fire({
            icon: 'success',
            title: 'You are Now',
            text: 'Login!'
         });//for educational purposes only. 
           fetch(`https://evening-fjord-91994.herokuapp.com/api/users/details`, {
           	headers: {
           		'Authorization': `Bearer ${data.access}`
           	}
           }).then(res => {
           	  return res.json()
           }).then(data => {
           	  localStorage.setItem("id", data._id) //this came from the payload.
           	  localStorage.setItem("isAdmin", data.isAdmin)
           	  console.log("items are set inside the local storage.")
              //direct the user to the courses page upon successful login.
              window.location.replace('./courses.html')
           })
      	}else{
           //if there is no exixting access key value from the data variable then just inform the user.
           Swal.fire({
            icon: 'error',
            title: 'Ooops!',
            text: 'make sure everything is filled up correctly'
         });
      	}
      })
   }
})