function validation() {
	var registerUser = document.getElementById("registerUser");
    let userEmail = document.querySelector("#userEmail").value;
	let text = document.getElementById("text");
	let pattern = /^[^ ]+@[^ ]+\.[a-z]{2,3}$/;

	if(userEmail.match(pattern))
	{
		registerUser.classList.add("valid");
		registerUser.classList.remove("invalid");
        registerUser.disabled = false;
		text.innerHTML = "Your Email Address is Valid";
		text.style.color = "#00ff00";
	}
	else
	{
		registerUser.classList.remove("valid");
		registerUser.classList.add("invalid");
        registerUser.disabled = true;
		text.innerHTML = "Please Enter a Valid Email Address";
		text.style.color = "#ff0000";


	}
	if(userEmail == "")
	{
		registerUser.classList.remove("valid");
		registerUser.classList.remove("invalid");
		text.innerHTML = "Please Enter a Valid Email Address";
		text.style.color = "#00ff00";
	}

}