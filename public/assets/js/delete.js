// alert("hello");

let params = new URLSearchParams(window.location.search)

let id = params.get("courseId");

console.log(id)



let name = document.querySelector("#courseName");
let desc = document.querySelector("#courseDesc");
let price = document.querySelector("#coursePrice");


fetch(`https://evening-fjord-91994.herokuapp.com/api/courses/${id}`).then(res => res.json()).then(data => {
    console.log(data)

    name.innerHTML = data.name
    desc.innerHTML = data.description
    price.innerHTML = data.price
})

// let y = "are your sure you want to delete this?";

document.querySelector("#delete").addEventListener("click", (e) =>{
    e.preventDefault();
  
    const deleteMethod = {
        method: 'DELETE', // Method itself
        headers: {
         'Content-type': 'application/json; charset=UTF-8' // Indicates the content 
        },
        // No need to have body, because we don't send nothing to the server.
       }
       // Make the HTTP Delete call using fetch api
       fetch(`https://evening-fjord-91994.herokuapp.com/api/courses/delete/${id}`,{
        method: 'DELETE',
        })
        
        .then(res => {
            return res.json()
         }).then(data => {
              console.log(data)
                if(data === true){
                    Swal.fire({
                        icon: 'success',
                        title: 'Great!',
                        text: 'you have successfully removed a course'
                     });
            } else{
                Swal.fire({
                    icon: 'success',
                    title: 'Great!',
                    text: 'you have successfully removed a course'
                 });
           } 
      })
    })  
    