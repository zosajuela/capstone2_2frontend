let params = new URLSearchParams(window.location.search)
//window.location -> returns a location object with information about the "current" location of the document. 

let id = params.get('courseId')
console.log(id)

let token = localStorage.getItem('token')
console.log(token)

let name = document.querySelector("#courseName")
let desc = document.querySelector("#courseDesc")
let price = document.querySelector("#coursePrice")
let enroll = document.querySelector("#enrollmentContainer")

fetch(`http://localhost:4000/api/courses/${id}`).then(res => res.json()).then(data => {
    console.log(data)

    name.innerHTML = data.name
    desc.innerHTML = data.description
    price.innerHTML = data.price   
    enroll.innerHTML = `<a id="enrollButton" class="btn btn-success text-white btn-block">Enroll</a>` 
    //we have to capture first the anchor tag for enroll, add an event listener to trigger an event. create a function in the eventlistener() to describe the next set of procedures.
    document.querySelector("#enrollButton").addEventListener("click", () => {
        //insert the course to our enrollment array inside the user collection.
        fetch('http://localhost:4000/api/users/enroll', {
        	//describe the parts of the request.
        	method: 'POST',
        	headers: {
        		'Content-Type': 'application/json',
        		'Authorization': `Bearer ${token}` //we have to get the value of auth string. to verify and validate the user. 
        	},
        	body: JSON.stringify({
        		courseId: id
        	})
        })
        .then(res => {
        	return res.json()
                                                                                            })
        .then(data => {
           //now we can inform the user that the request has been done. or not.
           if(data === true){
              alert("Thank you for enrolling to this course.")
              //redirect back to courses page.
              window.location.replace("./courses.html")
           }else{
              //inform the user that the request has failed.
              alert("something went wrong.")
           }
        })  
    })
    //
}) //if the result is displayed in the console. it means you were able to properly pass the courseid and successfully created your fetch request. 



// document.querySelector("#deleted").addEventListener("click", () =>{
//     const deleteMethod = {
//         method: 'DELETE', // Method itself
//         headers: {
//          'Content-type': 'application/json; charset=UTF-8' // Indicates the content 
//         },
//         // No need to have body, because we don't send nothing to the server.
//        }
//        // Make the HTTP Delete call using fetch api
//        fetch(`https://evening-fjord-91994.herokuapp.com/api/courses/delete/${id}`,{
//         method: 'DELETE',
//         })
//         .then(res => {
//             return res.json()
//          }).then(data => {
//                location.reload()
//                 if(data === true){
//                alert("successfully remove Course, please refresh the page")
//             } else{
//                  alert("successfully remove Course, please refresh the page")
//            }
//       })
// //     })  
// fetch('http://localhost:4000/api/users/enroll', {
// 				method: 'POST',
// 				headers: {
// 					'Content-Type': 'application/json',
// 					Authorization: Bearer ${localStorage.getItem('token')},
// 				},
// 				body: JSON.stringify({
// 					courseId: cid,
// 				}),
// 			})
// 				.then((res) => res.json())
// 				.then((data) => {
// 					if (data === true) {
// 						Swal.fire({
// 							icon: 'success',
// 							title: 'Enrollment Successful!',
// 							showDenyButton: false,
// 							showCancelButton: false,
// 							confirmButtonText: OK,
// 						}).then((result) => {
// 							/* Read more about isConfirmed, isDenied below */
// 							if (result.isConfirmed) {
// 								window.location.replace(
// 									./course.html?courseId=${cid}
// 								);
// 							}
// 						});
// 					} else {
// 						Swal.fire({
// 							icon: 'error',
// 							title: 'Ooops! Something went wrong...',
// 							showDenyButton: false,
// 							showCancelButton: false,
// 							confirmButtonText: OK,
// 						}).then((result) => {
// 							/* Read more about isConfirmed, isDenied below */
// 							if (result.isConfirmed) {
// 								window.location.replace(
// 									./course.html?courseId=${cid}
// 								);
// 							}
// 						});
// 						Swal.fire({
// 							icon: 'error',
// 							title: 'Enrollment Failed!',
// 							text:
// 								'Oppps! An issue occurred while enrolling, please contact the administrator.',
// 						});
// 					}
// 				});