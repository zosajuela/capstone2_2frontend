let createCourse = document.querySelector("#createCourse");
        
// e=event
createCourse.addEventListener("submit", (e) => {
    e.preventDefault();

    let courseName = document.querySelector("#courseName").value;
    let coursePrice = document.querySelector("#coursePrice").value;
    let courseDescription = document.querySelector("#courseDescription").value;


    //fetch() has 2 params => route(destination)
    if(courseName !== "" && coursePrice !== "" && courseDescription !== ""){
        fetch('http://localhost:4000/api/courses/name-exists',{
            method: 'PATCH',
            headers:{
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: courseName
            })
            }).then(res => res.json()).then(data => {
             if(data === false){
                  fetch("http://localhost:4000/api/courses/addCourse",{
                     //built a structure of our request
                      method: 'POST', 
                      headers: {
                             'Content-Type': 'application/json'
                  },
                           body: JSON.stringify({
                            name: courseName,
                            description: courseDescription, 
                            price: coursePrice,
                  })
                 //create a promise to identify what will happen if res from back end is positive or nega
                    }).then(res => {
                           return res.json()
                        }).then(data => {
                              console.log(data)
                               if(data === true){
                              alert("New Course inserted succssfully")
                           } else{
                                alert("Something went wrong in the Registration")
                          }
                     })
            }else{
                    alert("course name already exists, choose another email")
            }
          })
    }else{
        alert("please make sure there are no blank page")
    }
})